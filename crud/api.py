from rest_framework import routers
from accounts.api.views import CommentViewSet, ProductsViewSet, CustomerViewSet

router = routers.DefaultRouter()
router.register(r'comments', CommentViewSet)
router.register(r'products', ProductsViewSet)
router.register(r'customers', CustomerViewSet, basename="customers")