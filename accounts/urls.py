from django.urls import path, include
from crud.api import router
from . import views


urlpatterns = [
    path('register/', views.registerPage, name="register"),
    path('login/', views.loginPage, name="login"),
    path('login-api/', views.login_user, name="login-api"),
    path('me/', views.user_me, name="me"),
    path('register-api/', views.register_user, name='register-api'),
    path('change-password/', views.change_password1, name='change-password'),
    path('logout/', views.logoutUser, name="logout"),


    path('', views.home, name="home"),
    path('user/', views.userPage, name='user-page'),
    path('account/', views.accountSettings, name='account'),
    path('products/', views.products, name="products"),


    path('customer/<str:pk>/', views.customer, name="customer"),
    path('create_order/<str:pk>/', views.createOrder, name="create_order"),
    path('create_order_user/', views.createOrderUser, name="create_order_user"),
    path('update_order/<str:pk>/', views.updateOrder, name="update_order"),
    path('delete_order/<str:pk>/', views.deleteOrder, name="delete_order"),
    path('comments/<int:id>/', views.CommentPage.as_view(), name='comments'),
    # path('comments/', views.CommentPage.as_view(), name='comments'),


    path('api/v1/',include(router.urls)),
]
