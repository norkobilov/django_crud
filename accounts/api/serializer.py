from rest_framework import routers, serializers, viewsets
from accounts.models import Comment, Product, Customer
# Serializers define the API representation.
from django.contrib.auth.models import User


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = "__all__"


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()

    class Meta:
        model = User
        # fields = ["id","last_login","username", "avatar"]
        fields = "__all__"

    def get_avatar(self, obj):
        return obj.username + " salom"


class CommentSerializer(serializers.ModelSerializer):
    replay = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = "__all__"

    def get_replay(self, obj):
        return CommentSerializer(obj.comment_replay, many=True).data

    def get_user(self, obj):
        return UserSerializer(obj.user, many=False).data
