from django.http import JsonResponse
from rest_framework import routers, serializers, viewsets
from rest_framework.decorators import action

from accounts.models import Comment, Product, Customer, User
from .serializer import CommentSerializer, ProductSerializer, CustomerSerializer, RegisterSerializer
from rest_framework.viewsets import mixins, GenericViewSet
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated


# class CustomTeacherCheck(IsAuthenticated):
#     def has_permission(self, request, view):
#         return (request.user)

class RegisterViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer


class CommentViewSet(mixins.ListModelMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Comment.objects.filter(replay__isnull=True).all()
    serializer_class = CommentSerializer
    permission_classes = [IsAuthenticated]


from rest_framework.permissions import AllowAny, IsAuthenticated


class ProductsViewSet(viewsets.ModelViewSet):
    permission_classes = [AllowAny]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer



class CustomerViewSet(viewsets.ModelViewSet):
    permission_classes = [AllowAny]
    queryset = Product.objects.all()
    serializer_class = CustomerSerializer
