import django_filters
from .models import *
from django_filters import DateFilter, CharFilter
from django import forms


class DateInput(forms.DateInput):
    input_type = 'date'


class OrderFilter(django_filters.FilterSet):
    start_date = DateFilter(field_name="date_created", widget=DateInput(attrs={'type': 'date'}), lookup_expr="gte",
                            label="Start Date")
    end_date = DateFilter(field_name="date_created", widget=DateInput(attrs={'type': 'date'}), lookup_expr="lte",
                          label="End Date")
    note = CharFilter(field_name='note', lookup_expr='icontains')

    class Meta:
        model = Order
        fields = '__all__'
        exclude = ['customer', 'date_created']
