from django.shortcuts import render, redirect
from django.http import HttpResponse, request, JsonResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
# Create your views here.
from django.urls import reverse
from django.views import View
from rest_framework.viewsets import ModelViewSet

from .api.serializer import RegisterSerializer
from .decorators import unauthenticated_user, allowed_users, admin_only
from .filters import OrderFilter
from .models import *
from .forms import OrderForm, CreateUserForm, CustomerForm, CommentForm


@unauthenticated_user
def registerPage(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.filter(name='customer').first()
            if group:
                user.groups.add(group)
            Customer.objects.create(
                user=user,
            )

            messages.success(request, 'Account was created for ' + username)
            return redirect('login')
    context = {'form': form}
    return render(request, 'accounts/register.html', context)


@unauthenticated_user
def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'Username or password is incorrect')

    context = {}
    return render(request, 'accounts/login.html', context)


def logoutUser(request):
    logout(request)
    return redirect('login')


from django.core.paginator import Paginator


@login_required(login_url='login')
@admin_only
def home(request):
    orders = Order.objects.all()
    paginator = Paginator(orders, 20)
    orders_data = paginator.get_page(1)
    customers = Customer.objects.all()

    total_customers = customers.count()

    total_orders = orders.count()
    delivered = orders.filter(status='Delivered').count()
    pending = orders.filter(status='Pending').count()

    context = {'orders': orders, 'customers': customers, 'orders_data': orders_data,
               'total_orders': total_orders, 'delivered': delivered,
               'pending': pending, 'total_customers': total_customers}
    return render(request, 'accounts/dashboard.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['customer'])
def userPage(request):
    orders = request.user.customer.order_set.all()

    total_orders = orders.count()
    delivered = orders.filter(status='Delivered').count()
    pending = orders.filter(status='Pending').count()

    print('ORDERS:', orders)

    context = {'orders': orders, 'total_orders': total_orders, 'delivered': delivered,
               'pending': pending}
    # context = {}
    return render(request, 'accounts/user.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['customer', 'admin'])
def accountSettings(request):
    customer = request.user.customer
    form = CustomerForm(instance=customer)

    if request.method == "POST":
        form = CustomerForm(request.POST, request.FILES, instance=customer)
        if form.is_valid():
            form.save()
    context = {'form': form}
    return render(request, 'accounts/account_settings.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'customer'])
def products(request):
    products = Product.objects.all()
    context = {'products': products}
    return render(request, 'accounts/products.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def customer(request, pk):
    customer = Customer.objects.get(id=pk)

    orders = customer.order_set.all()

    order_count = orders.count()

    myFilter = OrderFilter(request.GET, queryset=orders)
    orders = myFilter.qs

    context = {'customer': customer, 'orders': orders, 'order_count': order_count, 'myFilter': myFilter}
    return render(request, 'accounts/customer.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def createOrder(request, pk):
    OrderFormSet = inlineformset_factory(Customer, Order, fields=('product', 'status'), extra=1)
    customer = Customer.objects.get(id=pk)
    formset = OrderFormSet(queryset=Order.objects.none(), instance=customer)
    if request.method == 'POST':
        formset = OrderFormSet(request.POST, instance=customer)
        if formset.is_valid():
            formset.save()
            return redirect('/')

    context = {'formset': formset}
    return render(request, 'accounts/order_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['customer'])
def createOrderUser(request):
    OrderFormSet = inlineformset_factory(Customer, Order, fields=('product', 'status'), extra=1)
    customer = Customer.objects.get(user=request.user)
    formset = OrderFormSet(queryset=Order.objects.none(), instance=customer)
    if request.method == 'POST':
        formset = OrderFormSet(request.POST, instance=customer)
        if formset.is_valid():
            formset.save()
            return redirect('/')

    context = {'formset': formset}
    return render(request, 'accounts/order_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def updateOrder(request, pk):
    order = Order.objects.get(id=pk)
    form = OrderForm(instance=order)

    if request.method == 'POST':
        form = OrderForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            return redirect('/')
    context = {'formset': form}
    return render(request, 'accounts/order_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def deleteOrder(request, pk):
    order = Order.objects.get(id=pk)
    if request.method == "POST":
        order.delete()
        return redirect('/')
    context = {'item': order}
    return render(request, 'accounts/delete.html', context)


class CommentPage(View):
    def get(self, request, id, *args, **kwargs):
        form = CommentForm
        # print(request.GET)
        # print(id)
        comments = Comment.objects.filter(replay__isnull=True, product_id=id).all()
        context = {'form': form, 'comments': comments}
        return render(request, 'accounts/comments.html', context)

    def post(self, request, id, *args, **kwargs):
        if request.POST.get('content') and request.user:
            Comment.objects.create(
                content=request.POST.get('content'),
                user=request.user,
                product_id=id
            )
        return redirect('/comments/' + str(id) + '/')


from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

from django.contrib.auth.hashers import make_password


@api_view(["POST"])
@permission_classes([AllowAny])
def register_user(request):
    try:
        username = request.POST["username"]
        email = request.POST["email"]
        password = request.POST["password"]
        if User.objects.filter(username=username).exists():
            return JsonResponse({"msg": "This user already exist !"})
        else:
            user = User.objects.create_user(
                username=username,
                email=email
            )
            user.save()
            user.password = make_password(password)
            user.save()
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            return JsonResponse({"status": 1, "data": {"token": token, "user": user.username}})
    except KeyError:
        return JsonResponse({"status": 0, "msg": "Some data is wrong for register !"})


@api_view(["POST"])
@permission_classes([AllowAny])
def login_user(request):
    user = User.objects.filter(username=request.POST.get("login")).first()
    if user and user.check_password(request.POST.get("password")):
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return JsonResponse({"status": 1, "data": {"token": token, "user": user.username}})
    return JsonResponse({"status": 0, "msg": "Login or password not corrent"})


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def user_me(request):
    user = request.user
    return JsonResponse({"status": 1, "user": {"name": user.username}})


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def change_password1(request, *args, **kwargs):
    try:
        user = request.user
        old_password = request.data.get('old_password')
        new_password = request.data.get('new_password')
        if user.check_password(old_password):
            user.set_password(new_password)
            user.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'msg': 'Password updated successfully',
                'data': []
            }
            return JsonResponse(response)
        return JsonResponse({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
    except KeyError:
        return JsonResponse({"status": 0, "msg": "Some wrong !"})
