from django.shortcuts import render

from channels.layers import get_channel_layer

channel_layer = get_channel_layer()
from asgiref.sync import async_to_sync
from django.http import HttpResponse


def send_messge(request):
    async_to_sync(channel_layer.group_send)(request.GET.get("room_name"), {
        'type': 'chat_message',
        'message': request.GET.get("message")
    })

    return HttpResponse("ok")
